FROM debian
RUN apt-get update && apt-get install -y openssh-server sudo python3 vim 
RUN sed -i 's/#PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config 
ENTRYPOINT service ssh start && bash

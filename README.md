![](https://cdn-images-1.medium.com/fit/t/1600/480/1*AoK4pqVAILB1twMF3E98FQ.png)

# container-ssh
esse projeto é feito para testes de ssh com containers docker, ja levando em concideracao que ja tenha docker e anisble instalado e configurado.


## BUILDANDO IMAGEM 

- [ ] buildar a imagem com o comando:

```bash
$ docker image build -t ssh .
```

## SUBINDO E CONFIGURANDO CONTAINER

- [ ] subir o container:

```bash
$ docker container run -dit -p 22:22 --name debian-ssh --hostname debian-ssh ssh
```

- [ ] acessar container criar o usuario de acesso:

```bash
$ docker exec -it debian-ssh bash
# adduser deploy 
# passwd deploy
```
- [ ] executar teste ansible:

```bash
$ ansible -m ping ssh-debian
ssh-debian | SUCCESS => {
"ansible_facts": {
"discovered_interpreter_python": "/usr/bin/python3"
},
"changed": false,
"ping": "pong"
}
$
```
